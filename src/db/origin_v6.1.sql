/*
SQLyog  v12.2.6 (64 bit)
MySQL - 5.7.27 : Database - mydeom
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`mydeom` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `mydeom`;

/*Table structure for table `role` */

DROP TABLE IF EXISTS `role`;

CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(20) NOT NULL,
  `role_permission` varchar(200) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `role` */

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account` bigint(20) NOT NULL,
  `NAME` varchar(200) NOT NULL,
  `address` datetime NOT NULL,
  `age` int(11) DEFAULT '0' COMMENT '用户的年龄',
  `height` int(11) DEFAULT '0' COMMENT '用户身高',
  `health` int(11) DEFAULT '0' COMMENT '用户的身体健康状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

/*Data for the table `user` */

insert  into `user`(`id`,`account`,`NAME`,`address`,`age`,`height`,`health`) values 
(5,1111,'1111','2019-09-09 11:05:42',0,12,0),
(6,2222,'2222','2019-09-09 11:05:54',0,12,0),
(7,3333,'3333','2019-09-09 11:06:12',0,12,0);

/*Table structure for table `user_role` */

DROP TABLE IF EXISTS `user_role`;

CREATE TABLE `user_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(20) NOT NULL,
  `user_role_permission` varchar(200) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

/*Data for the table `user_role` */

/*Table structure for table `user_role_permission` */

DROP TABLE IF EXISTS `user_role_permission`;

CREATE TABLE `user_role_permission` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_role_name` varchar(20) NOT NULL,
  `user_role_permission` varchar(200) NOT NULL,
  `create_date` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;


/*Data for the table `user_role_permission` */
DROP TABLE IF EXISTS `mydom6.1`;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
