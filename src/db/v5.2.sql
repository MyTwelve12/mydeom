

use mydeom;
CREATE TABLE `user_role_permission` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `user_role_name` VARCHAR(20) NOT NULL,
  `user_role_permission` VARCHAR(200) NOT NULL,
  `create_date` DATETIME NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=INNODB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8