package ic.comtroller;

/**
 * Created by mayn on 2019/7/28.
 */
public class DogController {
    public String getDogName(int type){
        String name = "";
        switch (type){
            case 1:
                name="小狗的名字叫小宝";
                break;
            case 2:
                name="小狗的名字叫小明";
                break;
            case 3:
                name="小狗的名字叫小白";
                break;
            default:
                name = "小狗刚出生还未起名字";
                break;
        }


        return name;
    }

}
