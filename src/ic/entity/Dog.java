package ic.entity;

/**
 * Created by mayn on 2019/7/28.
 */
public class Dog {
    //名字
    private String name;
    //年龄
    private int age;

    //体重
    private int weight;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
}
